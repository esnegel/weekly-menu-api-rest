package com.snegrete.menu;

import com.google.gson.Gson;
import com.snegrete.menu.api.ingredient.model.Ingredient;
import com.snegrete.menu.api.ingredient.model.types.FoodGroup;
import com.snegrete.menu.api.ingredient.model.types.PurchaseCenter;
import com.snegrete.menu.api.recipe.model.Measure;
import com.snegrete.menu.api.recipe.model.Recipe;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashMap;

@SpringBootApplication
public class MenuApplication {

    public static void main(String[] args) {
        SpringApplication.run(MenuApplication.class, args);
    }

}
