package com.snegrete.menu.api.base;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface BaseApi<T, I> {

    ResponseEntity<I> save(T ingredient);

    ResponseEntity<List<T>> find(T ingredient);

    ResponseEntity<List<T>> findAll();

    ResponseEntity<T> get(I id);

    ResponseEntity<Void> delete(I id);
}
