package com.snegrete.menu.api.base.model;

import lombok.Getter;
import org.bson.codecs.pojo.annotations.BsonId;

@Getter
public abstract class BaseDocument<T> {

    @BsonId
    private T id;
}
