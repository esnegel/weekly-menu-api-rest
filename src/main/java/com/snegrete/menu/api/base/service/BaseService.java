package com.snegrete.menu.api.base.service;

import java.util.List;

public interface BaseService<T, I> {

    I save(T ingredient);

    List<T> find(T ingredient);

    List<T> findAll();

    T get(I id);

    void delete(I id);
}
