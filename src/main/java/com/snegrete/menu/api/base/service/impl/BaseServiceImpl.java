package com.snegrete.menu.api.base.service.impl;

import com.snegrete.menu.api.base.model.BaseDocument;
import com.snegrete.menu.api.base.repository.BaseRepository;
import com.snegrete.menu.api.base.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;

import java.util.List;

@AllArgsConstructor
public class BaseServiceImpl<T extends BaseDocument<I>, I, R extends BaseRepository<T, I>> implements BaseService<T, I> {

    private final R repository;

    @Override
    public I save(T obj) {
        return repository.save(obj).getId();
    }

    @Override
    public List<T> find(T obj) {
        return repository.findAll(Example.of(obj));
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public T get(I id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void delete(I id) {
        repository.deleteById(id);
    }
}
