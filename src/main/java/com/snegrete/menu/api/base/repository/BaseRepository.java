package com.snegrete.menu.api.base.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<T, I> extends MongoRepository<T, I> {

    Optional<T> findById(I id);

    void deleteById(I id);
}
