package com.snegrete.menu.api.base.controller;

import com.snegrete.menu.api.base.BaseApi;
import com.snegrete.menu.api.base.model.BaseDocument;
import com.snegrete.menu.api.base.service.BaseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
public class BaseController<T extends BaseDocument<I>, I, S extends BaseService<T, I>> implements BaseApi<T, I> {

    private final S service;

    @Override
    @PutMapping
    public ResponseEntity<I> save(@RequestBody T obj) {
        return new ResponseEntity<>(service.save(obj), HttpStatus.OK);
    }

    @Override
    @PostMapping
    public ResponseEntity<List<T>> find(@RequestBody T obj) {
        return new ResponseEntity<>(service.find(obj), HttpStatus.OK);
    }

    @Override
    @GetMapping
    public ResponseEntity<List<T>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @Override
    @GetMapping("{id}")
    public ResponseEntity<T> get(@PathVariable I id) {
        return new ResponseEntity<>(service.get(id), HttpStatus.OK);
    }

    @Override
    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable I id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
