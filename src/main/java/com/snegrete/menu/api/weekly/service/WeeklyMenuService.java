package com.snegrete.menu.api.weekly.service;

import com.snegrete.menu.api.base.service.BaseService;
import com.snegrete.menu.api.weekly.model.WeeklyMenu;

public interface WeeklyMenuService extends BaseService<WeeklyMenu, String> {
}
