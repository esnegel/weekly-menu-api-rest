package com.snegrete.menu.api.weekly.repository;

import com.snegrete.menu.api.base.repository.BaseRepository;
import com.snegrete.menu.api.weekly.model.WeeklyMenu;

public interface WeeklyMenuRepository extends BaseRepository<WeeklyMenu, String> {
}
