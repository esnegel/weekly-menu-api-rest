package com.snegrete.menu.api.weekly.service.impl;

import com.snegrete.menu.api.base.service.impl.BaseServiceImpl;
import com.snegrete.menu.api.weekly.model.WeeklyMenu;
import com.snegrete.menu.api.weekly.repository.WeeklyMenuRepository;
import com.snegrete.menu.api.weekly.service.WeeklyMenuService;
import org.springframework.stereotype.Service;

@Service
public class WeeklyMenuServiceImpl extends BaseServiceImpl<WeeklyMenu, String, WeeklyMenuRepository> implements WeeklyMenuService {

    public WeeklyMenuServiceImpl(WeeklyMenuRepository repository) {
        super(repository);
    }
}
