package com.snegrete.menu.api.weekly.model;

import com.snegrete.menu.api.base.model.BaseDocument;
import com.snegrete.menu.api.recipe.model.Recipe;
import com.snegrete.menu.api.recipe.model.types.Lunch;
import lombok.Getter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.DayOfWeek;
import java.util.EnumMap;

@Document
public class WeeklyMenu extends BaseDocument<String> {

    @Getter
    private EnumMap<DayOfWeek, EnumMap<Lunch, Recipe>> lunches;
}
