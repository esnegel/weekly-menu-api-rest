package com.snegrete.menu.api.weekly.controller;

import com.snegrete.menu.api.base.controller.BaseController;
import com.snegrete.menu.api.weekly.model.WeeklyMenu;
import com.snegrete.menu.api.weekly.service.WeeklyMenuService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("menu")
public class WeeklyMenuController extends BaseController<WeeklyMenu, String, WeeklyMenuService> {

    public WeeklyMenuController(WeeklyMenuService service) {
        super(service);
    }
}
