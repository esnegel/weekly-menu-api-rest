package com.snegrete.menu.api.ingredient.model.types;

public enum FoodGroup {
    MILK, LEGUME, CEREAL, MEAT, FISH, EGG, FRUIT, VEGETABLES, SUGAR, SPICES;
}
