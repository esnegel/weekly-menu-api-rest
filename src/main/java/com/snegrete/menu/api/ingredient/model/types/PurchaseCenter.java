package com.snegrete.menu.api.ingredient.model.types;

public enum PurchaseCenter {
    MERCADONA, ALCAMPO, FRUIT_STORE, BUTCHERY;
}
