package com.snegrete.menu.api.ingredient.service;

import com.snegrete.menu.api.base.service.BaseService;
import com.snegrete.menu.api.ingredient.model.Ingredient;

public interface IngredientService extends BaseService<Ingredient, String> {
}
