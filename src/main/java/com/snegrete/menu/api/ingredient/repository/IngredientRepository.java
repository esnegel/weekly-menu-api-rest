package com.snegrete.menu.api.ingredient.repository;

import com.snegrete.menu.api.ingredient.model.Ingredient;
import com.snegrete.menu.api.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends BaseRepository<Ingredient, String> {
}
