package com.snegrete.menu.api.ingredient.controller;

import com.snegrete.menu.api.base.controller.BaseController;
import com.snegrete.menu.api.ingredient.IngredientApi;
import com.snegrete.menu.api.ingredient.repository.IngredientRepository;
import com.snegrete.menu.api.ingredient.model.Ingredient;
import com.snegrete.menu.api.ingredient.service.IngredientService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ingredient")
public class IngredientApiController extends BaseController<Ingredient, String, IngredientService> implements IngredientApi {

    public IngredientApiController(IngredientService service) {
        super(service);
    }
}
