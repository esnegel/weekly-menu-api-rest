package com.snegrete.menu.api.ingredient.model;

import com.snegrete.menu.api.base.model.BaseDocument;
import com.snegrete.menu.api.ingredient.model.types.FoodGroup;
import com.snegrete.menu.api.ingredient.model.types.PurchaseCenter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@EqualsAndHashCode(callSuper = true)
public class Ingredient extends BaseDocument<String> {

    private String name;

    private FoodGroup group;

    private PurchaseCenter purchaseCenter;

    private Float price;
}
