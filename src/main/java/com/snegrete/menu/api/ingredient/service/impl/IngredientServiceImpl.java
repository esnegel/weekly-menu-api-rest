package com.snegrete.menu.api.ingredient.service.impl;

import com.snegrete.menu.api.base.service.impl.BaseServiceImpl;
import com.snegrete.menu.api.ingredient.repository.IngredientRepository;
import com.snegrete.menu.api.ingredient.service.IngredientService;
import com.snegrete.menu.api.ingredient.model.Ingredient;
import org.springframework.stereotype.Service;

@Service
public class IngredientServiceImpl extends BaseServiceImpl<Ingredient, String, IngredientRepository> implements IngredientService {

    public IngredientServiceImpl(IngredientRepository repository) {
        super(repository);
    }
}
