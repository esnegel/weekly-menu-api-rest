package com.snegrete.menu.api.ingredient;

import com.snegrete.menu.api.base.BaseApi;
import com.snegrete.menu.api.ingredient.model.Ingredient;

public interface IngredientApi extends BaseApi<Ingredient, String> {
}
