package com.snegrete.menu.api.recipe.controller;

import com.snegrete.menu.api.base.controller.BaseController;
import com.snegrete.menu.api.recipe.service.RecipeService;
import com.snegrete.menu.api.recipe.service.impl.RecipeServiceImpl;
import com.snegrete.menu.api.recipe.model.Recipe;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/recipe")
public class RecipeApiController extends BaseController<Recipe, String, RecipeService> {

    public RecipeApiController(RecipeService service) {
        super(service);
    }
}
