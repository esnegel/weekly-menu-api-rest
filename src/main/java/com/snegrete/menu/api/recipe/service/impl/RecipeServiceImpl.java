package com.snegrete.menu.api.recipe.service.impl;

import com.snegrete.menu.api.base.service.impl.BaseServiceImpl;
import com.snegrete.menu.api.recipe.repository.RecipeRepository;
import com.snegrete.menu.api.recipe.service.RecipeService;
import com.snegrete.menu.api.recipe.model.Recipe;
import org.springframework.stereotype.Service;

@Service
public class RecipeServiceImpl extends BaseServiceImpl<Recipe, String, RecipeRepository> implements RecipeService {

    public RecipeServiceImpl(RecipeRepository repository) {
        super(repository);
    }
}
