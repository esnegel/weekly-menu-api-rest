package com.snegrete.menu.api.recipe.model;

import lombok.Data;

@Data
public class Measure {

    private Integer amount;

    private String measurementUnit;
}
