package com.snegrete.menu.api.recipe;

import com.snegrete.menu.api.base.BaseApi;
import com.snegrete.menu.api.recipe.model.Recipe;

public interface RecipeApi extends BaseApi<Recipe, String> {
}
