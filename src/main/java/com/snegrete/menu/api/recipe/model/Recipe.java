package com.snegrete.menu.api.recipe.model;

import com.snegrete.menu.api.base.model.BaseDocument;
import com.snegrete.menu.api.ingredient.model.Ingredient;
import com.snegrete.menu.api.recipe.model.types.Lunch;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
@EqualsAndHashCode(callSuper = true)
public class Recipe extends BaseDocument<String> {

    private String name;

    private List<Ingredient> mainIngredients;

    private List<MeasuredIngredient> ingredients;

    private List<Step> steps;

    private List<Lunch> lunchUsages;
}
