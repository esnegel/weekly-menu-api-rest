package com.snegrete.menu.api.recipe.service;

import com.snegrete.menu.api.base.service.BaseService;
import com.snegrete.menu.api.recipe.model.Recipe;

public interface RecipeService extends BaseService<Recipe, String> {
}
