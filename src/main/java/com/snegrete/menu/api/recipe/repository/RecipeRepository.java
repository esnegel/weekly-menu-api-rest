package com.snegrete.menu.api.recipe.repository;

import com.snegrete.menu.api.base.repository.BaseRepository;
import com.snegrete.menu.api.recipe.model.Recipe;

public interface RecipeRepository extends BaseRepository<Recipe, String> {
}
