package com.snegrete.menu.api.recipe.model;

import lombok.Data;

@Data
public class Step {

    private Integer order;

    private String description;
}
