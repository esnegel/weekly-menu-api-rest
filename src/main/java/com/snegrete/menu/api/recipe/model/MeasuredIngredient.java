package com.snegrete.menu.api.recipe.model;

import com.snegrete.menu.api.ingredient.model.Ingredient;
import lombok.Data;

@Data
public class MeasuredIngredient {

    private Ingredient ingredient;

    private Measure measure;
}
